const express = require('express');
const pino = require('pino');
const path = require('path');
const fs = require('fs');
// importing prom-client
const client = require('prom-client');

const app = express();

const collectDefaultMetrics = client.collectDefaultMetrics;
// Probe every 5 secs
collectDefaultMetrics({ timeout: 5000 });

// Define/ register 2 metrics of interest
const httpRequestsTotal = new client.Counter({
  name: 'http_request_operations_total',
  help: 'Total number of http requests',
});

const httpRequestDurationSeconds = new client.Histogram({
  name: 'http_request_duration_seconds',
  help: 'Duration of http requests in seconds',
  buckets: [0.1, 0.5, 2, 5, 10],
});

// route exposes calculated/ collected metrics
app.get('/metrics', async (req, res) => {
  // get metrics data type (time series format) from client
  res.set('Content-Type', client.register.contentType);
  // send collected metrics data to browser
  res.end(await client.register.metrics());
});

const logger = pino({
  level: 'info',
  timestamp: () => `,"time":"${new Date().toISOString()}"`,
});

app.get('/', function (req, res) {
  // Simulate delay (a random num of millisecs) in response
  // Track time req entered route
  var start = new Date();
  var simulateTime = Math.floor(Math.random() * (10000 - 500 + 1) + 500);
  setTimeout(function (argument) {
    // Simulate req handling time
    var end = new Date() - start;
    // Convert req handling time to secs
    // Add observation to a bucket
    httpRequestDurationSeconds.observe(end / 1000);
  }, simulateTime);
  // increment total num reqs before sending res
  httpRequestsTotal.inc();
  res.send('Hello TWN students!');
});

app.listen(3000, function () {
  logger.info('app listening on port 3000!');
});
